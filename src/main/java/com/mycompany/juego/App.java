package com.mycompany.juego;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.image.Image;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 1017, 571);
        String cssPath = getClass().getResource("/css/css.css").toExternalForm();
        scene.getStylesheets().add(cssPath);
        stage.sizeToScene();
        stage.setResizable(false);
        stage.setScene(scene);
        stage.setTitle("Dege Battle");
        Image icon = new Image(getClass().getResourceAsStream("/Imagenes/degetet.png"));
        stage.getIcons().add(icon);
        stage.show();
        
        
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}