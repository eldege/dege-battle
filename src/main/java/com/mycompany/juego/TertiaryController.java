/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.juego;

import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.NodeOrientation;
import javafx.scene.Parent;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.util.Duration;

/**
 *
 * @author Usuario
 */
public class TertiaryController implements Initializable {
    
    @FXML
    private AnchorPane a;

    @FXML
    private ImageView fondo;
    
    private String escenario;
    
    @FXML
    private ImageView p1;

    @FXML
    private ImageView p2;
    
    private String personaje1;
    private String personaje2;
    
    @FXML
    private double posicionx;
    
    @FXML
    private double posiciony;
    
    @FXML
    private ImageView at11;

    @FXML
    private ImageView at21;

    @FXML
    private ImageView transformacion1;

    @FXML
    private ImageView at12;

    @FXML
    private ImageView at22;

    @FXML
    private ImageView transformacion2;

    @FXML
    private ProgressBar vida1;

    @FXML
    private ProgressBar vida2;
    
    @FXML
    private boolean transformacion1Activa = false;
    
    @FXML
    private boolean transformacion2Activa = false;
    
    @FXML
    private double widthIchigo;
    
    @FXML
    private double heightIchigo;
    
    @FXML
    private Text volver1;

    @FXML
    private Text volver2;
    
    @FXML
    private TranslateTransition luffyDash;
    
    @FXML
    private TranslateTransition snakeDash;
    
    @FXML
    private TranslateTransition ichigoDash;
    
    @FXML
    private Text nombre1 = null;

    @FXML
    private Text nombre2 = null;
    
    @FXML
    private Personaje objeto1;
    
    @FXML
    private Personaje objeto2;
    
    @FXML
    private ImageView critico1;

    @FXML
    private ImageView critico2;
    
    @FXML
    private ImageView hit1;

    @FXML
    private ImageView hit2;
    
    @FXML
    private ImageView coinflip;
    
    @FXML
    private ImageView negro;
    
    @FXML
    private boolean clickcoin = false;
    
    @FXML
    private boolean clickcoin2 = false;
    
    @FXML
    private int ataques1 = 0;
    
    @FXML
    private int ataques2 = 0;
    
    @FXML
    private Text numerovida1;

    @FXML
    private Text numerovida2;
    
    @FXML
    private ImageView ko;
    
    @FXML
    private String ganador;
   
   

    
    @FXML
    public void setEscenario (String escenario) {
    this.escenario = escenario;
    
    if ("stage1".equals(escenario)){
    Image bg = new Image(getClass().getResourceAsStream("/Imagenes/dresrossa.png"));
        fondo.setImage(bg);
    } else if ("stage2".equals(escenario)) {
    Image bg = new Image(getClass().getResourceAsStream("/Imagenes/bosque.png"));
        fondo.setImage(bg);
    } else if ("stage3".equals(escenario)) {
    Image bg = new Image(getClass().getResourceAsStream("/Imagenes/roof.png"));
        fondo.setImage(bg);
    }
    }
    
    @FXML
    public void setPersonajes (String personaje1,String personaje2) {
    this.personaje1 = personaje1;
    this.personaje2 = personaje2;
    
    if ("Luffy".equals(personaje1)) {
        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
        p1.setImage(personaje);
        posicionx = p1.getTranslateX();
        posiciony = p1.getTranslateY();
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p1.getFitWidth() / 2);
        p1.getTransforms().setAll(flipScale);
        p1.setTranslateX(posicionx-310);
        p1.setTranslateY(posiciony+5);
        nombre1.setText("LUFFY");
        objeto1 = new Personaje ("Luffy",100.0,0,false);
        vida1.setProgress(1);
        vida1.setStyle("-fx-accent: #3b547a");

    } else if ("Snake".equals(personaje1)) {
        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
        p1.setImage(personaje);
        posicionx = p1.getTranslateX();
        posiciony = p1.getTranslateY();
        p1.setTranslateX(posicionx+50);
        nombre1.setText("SNAKE");
        objeto1 = new Personaje ("Snake",100,0,false);
        vida1.setProgress(1);
        vida1.setStyle("-fx-accent:#3b547a");

    } else if ("Ichigo".equals(personaje1)) {
        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
        p1.setImage(personaje);
        posicionx = p1.getTranslateX();
        posiciony = p1.getTranslateY();
        p1.setTranslateX(posicionx+30);
        p1.setTranslateY(posiciony+13);
        widthIchigo = personaje.getWidth();
        heightIchigo = personaje.getHeight();
        nombre1.setText("ICHIGO");
        objeto1 = new Personaje ("Ichigo",100,0,false);
        vida1.setProgress(1);
        vida1.setStyle("-fx-accent:#3b547a");

    }
    
        if ("Luffy".equals(personaje2)) {
        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
        p2.setImage(personaje);
        posicionx = p2.getTranslateX();
        posiciony = p2.getTranslateY();
        p2.setTranslateX(posicionx+310);
        nombre2.setText("LUFFY");
        objeto2 = new Personaje ("Luffy",100,0,false);
        vida2.setProgress(1);
        vida2.setStyle("-fx-accent:#3b547a");

    } else if ("Snake".equals(personaje2)) {
        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
        p2.setImage(personaje);
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p2.getFitWidth() / 2);
        p2.getTransforms().setAll(flipScale);
        posicionx = p2.getTranslateX();
        posiciony = p2.getTranslateY();
        p2.setTranslateX(posicionx-50);
        p2.setTranslateY(posiciony);
        nombre2.setText("SNAKE");
        objeto2 = new Personaje ("Snake",100,0,false);
        vida2.setProgress(1);
        vida2.setStyle("-fx-accent:#3b547a");

    } else if ("Ichigo".equals(personaje2)) {
        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
        p2.setImage(personaje);
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p2.getFitWidth() / 2);
        p2.getTransforms().setAll(flipScale);
        posiciony = p2.getTranslateY();
        p2.setTranslateX(posicionx-30);
        p2.setTranslateY(posiciony+10);
        widthIchigo = personaje.getWidth();
        heightIchigo = personaje.getHeight();
        nombre2.setText("ICHIGO");
        objeto2 = new Personaje ("Ichigo",100,0,false);
        vida2.setProgress(1);
        vida2.setStyle("-fx-accent:#3b547a");

    }
    }
    
@FXML
public void transformacion1(MouseEvent event) {
    if (transformacion1Activa) {

        setPersonaje1(personaje1);
        transformacion1Activa = false;
    } else {

        if ("Luffy".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xluffygear3inv.gif"));
            p1.setImage(transformacion);
        } else if ("Snake".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xsnakepistola.gif"));
            p1.setImage(transformacion);
        } else if ("Ichigo".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xichigohollow.gif"));
            p1.setImage(transformacion);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformado.gif"));
            Duration gifDuration = Duration.millis(1450);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                }
            
            }));
            
            timeline.play();
        }


        transformacion1Activa = true;
    }
}
    
@FXML
public void transformacion2(MouseEvent event) {
    if (transformacion2Activa) {

        setPersonaje2(personaje2);
        transformacion2Activa = false;
    } else {

        if ("Luffy".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xluffygear3inv.gif"));
            p2.setImage(transformacion);
        } else if ("Snake".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xsnakepistola.gif"));
            p2.setImage(transformacion);
        } else if ("Ichigo".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xichigohollow.gif"));
            p2.setImage(transformacion);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformado.gif"));
            Duration gifDuration = Duration.millis(1450);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                }
            
            }));
            
            timeline.play();
            
        }


        transformacion2Activa = true;
    }
}


@FXML
public void atacar1p1 (MouseEvent event) {
    if (transformacion1Activa) {
            if ("Luffy".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffytransformadoat1.gif"));
            double posicion;
            posicion = p1.getTranslateX();
            p1.setImage(transformacion);
            p1.setTranslateX(posicion+600);
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xluffygear3quieto.png"));
            Duration gifDuration = Duration.millis(1000);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateX(posicion);

                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.25 - 0.15 + 0.01) * 100.0) + 0.15 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.18 && daño<=0.22) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.22 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
            
            
            
        } else if ("Snake".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakepistolaat1.gif"));
            p1.setImage(transformacion);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xsnakepistolaquieto.png"));
            Duration gifDuration = Duration.millis(500);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.25 - 0.15 + 0.01) * 100.0) + 0.15 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.18 && daño<=0.22) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.22 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
        } else if ("Ichigo".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformadoat1.gif"));
            double posiciony;
            double posicionx;
            posiciony = p1.getTranslateY();
            posicionx = p1.getTranslateX();
            p1.setImage(transformacion);
            p1.setTranslateY(posiciony-30);
            p1.setTranslateX(posicionx+650);
            
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformado.gif"));
            Duration gifDuration = Duration.millis(650);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateY(posiciony);
                    p1.setTranslateX(posicionx);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.25 - 0.15 + 0.01) * 100.0) + 0.15 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.18 && daño<=0.22) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.22 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
        }
    }
    
    if (!transformacion1Activa) {
                if ("Luffy".equals(personaje1)) {
                    
            
            Image ataque = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffyat1.gif"));
            double posicion;
            posicion = p1.getTranslateX();
            p1.setImage(ataque);
            p1.setTranslateX(posicion+600);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
            Duration gifDuration2 = Duration.millis(800);
            Timeline timeline2 = new Timeline(new KeyFrame(gifDuration2, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateX(posicion);
                }
            
            }));
            
            timeline2.play();
    
            
            double daño = (Math.floor(Math.random() * (0.1 - 0.05 + 0.01) * 100.0) + 0.05 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.06) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.06 && daño<=0.08) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.08 && daño<=0.1) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
        } else if ("Snake".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakeat1.gif"));
            p1.setImage(transformacion);
            double posicion;
            posicion = p1.getTranslateX();
            p1.setTranslateX(posicion+620);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
            Duration gifDuration = Duration.millis(800);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateX(posicion);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.1 - 0.05 + 0.01) * 100.0) + 0.05 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.06) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.06 && daño<=0.08) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.08 && daño<=0.1) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
        } else if ("Ichigo".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigoat1.gif"));
            double posiciony;
            double posicionx;
            posiciony = p1.getTranslateY();
            posicionx = p1.getTranslateX();
            p1.setImage(transformacion);
            p1.setTranslateY(posiciony-30);
            p1.setTranslateX(posicionx+650);
            
                                    Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
            Duration gifDuration = Duration.seconds(1);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateY(posiciony);
                    p1.setTranslateX(posicionx);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.1 - 0.05 + 0.01) * 100.0) + 0.05 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.06) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.06 && daño<=0.08) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.08 && daño<=0.1) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
            
        }
    }

}

@FXML
public void atacar2p1 (MouseEvent event) {
    if (transformacion1Activa) {
            if ("Luffy".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffytransformadoat2.gif"));
            p1.setImage(transformacion);
            double posicionx;
            double posiciony;
            posicionx = p1.getTranslateX();
            posiciony = p1.getTranslateY();
            p1.setTranslateX(posicionx+600);
            p1.setTranslateY(posiciony-20);
            
                        p1.setImage(transformacion);
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xluffygear3quieto.png"));
            Duration gifDuration = Duration.millis(1400);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateX(posicionx);
                    p1.setTranslateY(posiciony);

                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.3 - 0.1 + 0.01) * 100.0) + 0.1 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.18 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.25 && daño<=0.3) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
        } else if ("Snake".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakepistolaat2.gif"));
            p1.setImage(transformacion);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xsnakepistola.gif"));
            Duration gifDuration = Duration.millis(1200);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.3 - 0.1 + 0.01) * 100.0) + 0.1 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.18 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.25 && daño<=0.3) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
            
        } else if ("Ichigo".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformadoat2.gif"));
            
            double posiciony;
            double posicionx;
            posiciony = p1.getTranslateY();
            posicionx = p1.getTranslateX();
            p1.setImage(transformacion);
            p1.setTranslateY(posiciony-30);
            p1.setTranslateX(posicionx+650);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformado.gif"));
            Duration gifDuration = Duration.millis(650);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateY(posiciony);
                    p1.setTranslateX(posicionx);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.3 - 0.1 + 0.01) * 100.0) + 0.1 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.18 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.25 && daño<=0.3) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
            
            
            
            
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
            
        }
    }
    
        if (!transformacion1Activa) {
                if ("Luffy".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffyat2.gif"));
            p1.setImage(transformacion);
            double posicion;
            posicion = p1.getTranslateX();
            p1.setImage(transformacion);
            p1.setTranslateX(posicion+600);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
            Duration gifDuration = Duration.millis(1200);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateX(posicion);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.2 - 0.01 + 0.01) * 100.0) + 0.01 * 100.0) / 100.0;
            
            if (daño>0 && daño<=0.07) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.07 && daño<=0.14) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.14 && daño<=0.2) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }


            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
            
        } else if ("Snake".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakeat2.gif"));
            p1.setImage(transformacion);
            double posicion;
            posicion = p1.getTranslateX();
            p1.setTranslateX(posicion+620);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
            Duration gifDuration = Duration.millis(1000);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateX(posicion);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.2 - 0.01 + 0.01) * 100.0) + 0.01 * 100.0) / 100.0;
            
            if (daño>0 && daño<=0.07) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.07 && daño<=0.14) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.14 && daño<=0.2) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }


            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
        } else if ("Ichigo".equals(personaje1)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigoat2.gif"));

            double posiciony;
            double posicionx;
            posiciony = p1.getTranslateY();
            posicionx = p1.getTranslateX();
            p1.setImage(transformacion);
            p1.setTranslateY(posiciony-30);
            p1.setTranslateX(posicionx+650);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
            Duration gifDuration = Duration.millis(1300);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p1.setImage(personaje);
                    p1.setTranslateY(posiciony);
                    p1.setTranslateX(posicionx);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.2 - 0.01 + 0.01) * 100.0) + 0.01 * 100.0) / 100.0;
            
            if (daño>0 && daño<=0.07) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.07 && daño<=0.14) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            } else if (daño>0.14 && daño<=0.2) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit2.setImage(hit);
            mostrarHit(hit2);
            }


            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico2.setImage(crit);
            mostrarCritico(critico2);
            }
                
            System.out.println(daño);
            vida2.setProgress(vida2.getProgress()-daño);
            
            barravida2();
            turnoj1();
            
            
        }
    }
    
    

}

@FXML
public void atacar1p2 (MouseEvent event) {
    if (transformacion2Activa) {
            if ("Luffy".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffytransformadoat1.gif"));
            double posicion;
            posicion = p2.getTranslateX();
            p2.setImage(transformacion);
            p2.setTranslateX(posicion-600);
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xluffygear3quieto.png"));
            Duration gifDuration = Duration.millis(1000);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateX(posicion);

                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.25 - 0.15 + 0.01) * 100.0) + 0.15 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.18 && daño<=0.22) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.22 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        } else if ("Snake".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakepistolaat1.gif"));
            p2.setImage(transformacion);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xsnakepistolaquieto.png"));
            Duration gifDuration = Duration.millis(500);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.25 - 0.15 + 0.01) * 100.0) + 0.15 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.18 && daño<=0.22) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.22 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        } else if ("Ichigo".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformadoat1.gif"));
            double posiciony;
            double posicionx;
            posiciony = p2.getTranslateY();
            posicionx = p2.getTranslateX();
            p2.setImage(transformacion);
            p2.setTranslateY(posiciony-30);
            p2.setTranslateX(posicionx-650);
            

            
                        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformado.gif"));
            Duration gifDuration = Duration.millis(650);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateY(posiciony);
                    p2.setTranslateX(posicionx);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.25 - 0.15 + 0.01) * 100.0) + 0.15 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.18 && daño<=0.22) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.22 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        }
    }
    
        if (!transformacion2Activa) {
                if ("Luffy".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffyat1.gif"));
            p2.setImage(transformacion);
            double posicion;
            posicion = p2.getTranslateX();
            p2.setImage(transformacion);
            p2.setTranslateX(posicion-600);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
            Duration gifDuration = Duration.millis(800);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateX(posicion);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.1 - 0.05 + 0.01) * 100.0) + 0.05 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.06) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.06 && daño<=0.08) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.08 && daño<=0.1) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        } else if ("Snake".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakeat1.gif"));
            p2.setImage(transformacion);
            double posicion;
            posicion = p2.getTranslateX();
            p2.setTranslateX(posicion-620);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
            Duration gifDuration = Duration.millis(800);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateX(posicion);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.1 - 0.05 + 0.01) * 100.0) + 0.05 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.06) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.06 && daño<=0.08) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.08 && daño<=0.1) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        } else if ("Ichigo".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigoat1.gif"));
            p2.setImage(transformacion);
            double posiciony;
            double posicionx;
            posiciony = p2.getTranslateY();
            posicionx = p2.getTranslateX();
            p2.setTranslateY(posiciony-30);
            p2.setTranslateX(posicionx-650);

            
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
            Duration gifDuration = Duration.millis(1000);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateY(posiciony);
                    p2.setTranslateX(posicionx);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.1 - 0.05 + 0.01) * 100.0) + 0.05 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.06) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.06 && daño<=0.08) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.08 && daño<=0.1) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        }
    }

}

@FXML
public void atacar2p2 (MouseEvent event) {
    if (transformacion2Activa) {
            if ("Luffy".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffytransformadoat2.gif"));
            p2.setImage(transformacion);
            double posicionx;
            double posiciony;
            posicionx = p2.getTranslateX();
            posiciony = p2.getTranslateY();
            p2.setTranslateX(posicionx-600);
            p2.setTranslateY(posiciony-20);
                        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xluffygear3quieto.png"));
            Duration gifDuration = Duration.millis(1400);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateX(posicionx);
                    p2.setTranslateY(posiciony);

                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.3 - 0.1 + 0.01) * 100.0) + 0.1 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.18 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.25 && daño<=0.3) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
            
        } else if ("Snake".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakepistolaat2.gif"));
            p2.setImage(transformacion);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xsnakepistola.gif"));
            Duration gifDuration = Duration.millis(1200);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.3 - 0.1 + 0.01) * 100.0) + 0.1 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.18 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.25 && daño<=0.3) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        } else if ("Ichigo".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformadoat2.gif"));
            p2.setImage(transformacion);
            double posiciony;
            double posicionx;
            posiciony = p2.getTranslateY();
            posicionx = p2.getTranslateX();
            p2.setTranslateY(posiciony-30);
            p2.setTranslateX(posicionx-650);
            
                        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigotransformado.gif"));
            Duration gifDuration = Duration.millis(650);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateY(posiciony);
                    p2.setTranslateX(posicionx);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.3 - 0.1 + 0.01) * 100.0) + 0.1 * 100.0) / 100.0;

            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (daño>0 && daño<=0.18) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.18 && daño<=0.25) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.25 && daño<=0.3) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }
            
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
            
            
            
            
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        }
    }
    
            if (!transformacion2Activa) {
                if ("Luffy".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffyat2.gif"));
            p2.setImage(transformacion);
            double posicion;
            posicion = p2.getTranslateX();
            p2.setTranslateX(posicion-600);
            
                        Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
            Duration gifDuration = Duration.millis(1200);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateX(posicion);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.2 - 0.01 + 0.01) * 100.0) + 0.01 * 100.0) / 100.0;
            
            if (daño>0 && daño<=0.07) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.07 && daño<=0.14) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.14 && daño<=0.2) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }


            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        } else if ("Snake".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakeat2.gif"));
            p2.setImage(transformacion);
            double posicion;
            posicion = p2.getTranslateX();
            p2.setTranslateX(posicion-620);
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
            Duration gifDuration = Duration.millis(1000);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateX(posicion);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.2 - 0.01 + 0.01) * 100.0) + 0.01 * 100.0) / 100.0;
            
            if (daño>0 && daño<=0.07) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.07 && daño<=0.14) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.14 && daño<=0.2) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }


            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            barravida1();
            turnoj2();
            
        } else if ("Ichigo".equals(personaje2)) {
            Image transformacion = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigoat2.gif"));
            p2.setImage(transformacion);
            double posiciony;
            double posicionx;
            posiciony = p2.getTranslateY();
            posicionx = p2.getTranslateX();
            p2.setTranslateY(posiciony-30);
            p2.setTranslateX(posicionx-650);

            
            
            Image personaje = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
            Duration gifDuration = Duration.millis(1300);
            Timeline timeline = new Timeline(new KeyFrame(gifDuration, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    p2.setImage(personaje);
                    p2.setTranslateY(posiciony);
                    p2.setTranslateX(posicionx);
                }
            
            }));
            
            timeline.play();
            
            double daño = (Math.floor(Math.random() * (0.2 - 0.01 + 0.01) * 100.0) + 0.01 * 100.0) / 100.0;
            
            if (daño>0 && daño<=0.07) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/bad.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.07 && daño<=0.14) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/good.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            } else if (daño>0.14 && daño<=0.2) {
            Image hit = new Image(getClass().getResourceAsStream("/Imagenes/sick.png"));
            hit1.setImage(hit);
            mostrarHit(hit1);
            }


            int critico = (int) (Math.random() * 5 + 1);
                    System.out.println(critico);


            System.out.println(daño);
            
            if (critico == 5) {
            daño = daño*2;
            Image crit = new Image(getClass().getResourceAsStream("/Imagenes/Criticalhit.png"));
            critico1.setImage(crit);
            mostrarCritico(critico1);
            }
                
            System.out.println(daño);
            vida1.setProgress(vida1.getProgress()-daño);
            
            
            barravida1();
            turnoj2();
            
        }
    }

}

@FXML
public void barravida1 () {
    if (vida1.getProgress()<=0.6 && vida1.getProgress()>0.3) {
            vida1.setStyle("-fx-accent:#f0e965");
            
            } else if (vida1.getProgress()<=0.3 && vida1.getProgress()>0) {
            vida1.setStyle("-fx-accent:#eb3838"); 
            } else if (vida1.getProgress()<=0) {
            vida1.setProgress(0);
            }
}

@FXML
public void barravida2 () {
    if (vida2.getProgress()<=0.6 && vida2.getProgress()>0.3) {
            vida2.setStyle("-fx-accent:#f0e965");
            } else if (vida2.getProgress()<=0.3 && vida2.getProgress()>0) {
            vida2.setStyle("-fx-accent:#eb3838"); 
            } else if (vida2.getProgress()<=0) {
            vida2.setProgress(0);
            }
}

@FXML
public void setPersonaje1(String personaje) {
    this.personaje1 = personaje;
    Image imagen = null;

    if ("Luffy".equals(personaje)) {
        imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
        posicionx = p1.getTranslateX();
        posiciony = p1.getTranslateY();
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p1.getFitWidth() / 2);
    } else if ("Snake".equals(personaje)) {
        imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
        posicionx = p1.getTranslateX();
        posiciony = p1.getTranslateY();
    } else if ("Ichigo".equals(personaje)) {
        imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
    }

    p1.setImage(imagen);
}

@FXML
public void setPersonaje2(String personaje) {
    this.personaje2 = personaje;
    Image imagen = null;

    if ("Luffy".equals(personaje)) {
        imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
        posicionx = p2.getTranslateX();
        posiciony = p2.getTranslateY();
    } else if ("Snake".equals(personaje)) {
        imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p2.getFitWidth() / 2);
        p2.getTransforms().setAll(flipScale);
        posicionx = p2.getTranslateX();
        posiciony = p2.getTranslateY();
    } else if ("Ichigo".equals(personaje)) {
        imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p2.getFitWidth() / 2);
        p2.getTransforms().setAll(flipScale);
        posiciony = p2.getTranslateY();
    }

    p2.setImage(imagen);
}

@FXML
public void volver1(MouseEvent event) {
    at11.setVisible(true);
    at21.setVisible(true);
    transformacion1.setVisible(true);
    volver1.setVisible(false);
    Image imagen = null;
    imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
    p1.setImage(imagen);
    transformacion1Activa = false;
}

@FXML
public void volver2(MouseEvent event) {
    at12.setVisible(true);
    at22.setVisible(true);
    transformacion2.setVisible(true);
    volver2.setVisible(false);
    Image imagen = null;
    imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy2.gif"));
    p2.setImage(imagen);
    transformacion2Activa = false;
}

    @FXML
    public static void mostrarCritico(ImageView imageView) {
        Duration duracion = Duration.millis(1500);

        FadeTransition transicion = new FadeTransition(duracion, imageView);

        transicion.setFromValue(1.0);
        transicion.setToValue(0.0);

        transicion.setOnFinished(event -> {
            imageView.setVisible(false);
        });
     
        imageView.setVisible(true);
        transicion.play();
        
        
    }
    
    @FXML
    public static void mostrarHit (ImageView imageView) {
        Duration duracion = Duration.millis(1500);

        FadeTransition transicion = new FadeTransition(duracion, imageView);

        transicion.setFromValue(1.0);
        transicion.setToValue(0.0);

        transicion.setOnFinished(event -> {
            imageView.setVisible(false);
        });
     
        imageView.setVisible(true);
        transicion.play();
    }
    
    @FXML
    public void coinflip (MouseEvent event) throws IOException {
        int numero = (int) (Math.random() * 2 + 1);
        System.out.println(numero);
        
        if (!clickcoin) {
        switch (numero) {
            case 1: 
                Image imagen1 = new Image(getClass().getResourceAsStream("/Imagenes/coin1.png")); 
                coinflip.setImage(imagen1);
                at11.setVisible(true);
                at21.setVisible(true);
                
                
                break;
                
            case 2:
                Image imagen2 = new Image(getClass().getResourceAsStream("/Imagenes/coin2.png")); 
                coinflip.setImage(imagen2);
                at12.setVisible(true);
                at22.setVisible(true);
                
                break;
        
        }
        }else if (clickcoin && !clickcoin2) {
        coinflip.setVisible(false);
        negro.setVisible(false);
        clickcoin2 = true;
        } else if (clickcoin && clickcoin2) {
        cambiarPantalla1(event
        );
        }
        
        clickcoin=true;
        
    }
    
    
    
    @FXML
    public void turnoj1() {
    at11.setVisible(false);
    at21.setVisible(false);
    volver1.setVisible(false);
    transformacion1.setVisible(false);
    at12.setVisible(true);
    at22.setVisible(true);
    ataques1++;
    if (ataques2>=3) {
    transformacion2.setVisible(true);
    }
    if (vida2.getProgress()<=0) {
    at11.setVisible(false);
    at21.setVisible(false);
    volver1.setVisible(false);
    transformacion1.setVisible(false);
    at12.setVisible(false);
    at22.setVisible(false);
    volver2.setVisible(false);
    transformacion2.setVisible(false);
    vida1.setVisible(false);
    vida2.setVisible(false);
    nombre1.setVisible(false);
    nombre2.setVisible(false);
    negro.setVisible(true);
    Image koo = new Image(getClass().getResourceAsStream("/Imagenes/ko.gif"));
    coinflip.setVisible(true);
    coinflip.setImage(koo);
    coinflip.setFitWidth(800);
    coinflip.setFitHeight(459);
    double posicionx = coinflip.getTranslateX();
    double posiciony = coinflip.getTranslateY();
    coinflip.setTranslateX(posicionx-320);
    coinflip.setTranslateY(posiciony-150);
    
    if("LUFFY".equals(nombre2.getText())) {
    Image imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffymuerte.gif"));
    p2.setImage(imagen);
    } else if ("ICHIGO".equals(nombre2.getText())) {
    Image imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigomuerte.gif"));
    p2.setImage(imagen);
    } else if ("SNAKE".equals(nombre2.getText())) {
    Image imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakemuerte.gif"));
    p2.setImage(imagen);
    }
    
    if("LUFFY".equals(nombre1.getText())) {
    ganador = "LUFFY";
    } else if ("ICHIGO".equals(nombre1.getText())) {
    ganador = "ICHIGO";
    } else if ("SNAKE".equals(nombre1.getText())) {
    ganador = "SNAKE";
    }
    
    }
    }
    
    @FXML
    public void turnoj2() {
    at12.setVisible(false);
    at22.setVisible(false);
    volver2.setVisible(false);
    transformacion2.setVisible(false);
    at11.setVisible(true);
    at21.setVisible(true);
    ataques2++;
    if (ataques1>=3) {
    transformacion1.setVisible(true);
    }
    if (vida1.getProgress()<=0) {
    at11.setVisible(false);
    at21.setVisible(false);
    volver1.setVisible(false);
    transformacion1.setVisible(false);
    at12.setVisible(false);
    at22.setVisible(false);
    volver2.setVisible(false);
    transformacion2.setVisible(false);
    vida1.setVisible(false);
    vida2.setVisible(false);
    nombre1.setVisible(false);
    nombre2.setVisible(false);
    negro.setVisible(true);
    Image koo = new Image(getClass().getResourceAsStream("/Imagenes/ko.gif"));
    coinflip.setImage(koo);
    coinflip.setVisible(true);
    coinflip.setFitWidth(800);
    coinflip.setFitHeight(459);
    double posicionx = coinflip.getTranslateX();
    double posiciony = coinflip.getTranslateY();
    coinflip.setTranslateX(posicionx-320);
    coinflip.setTranslateY(posiciony-150);
    
    if("LUFFY".equals(nombre1.getText())) {
    Image imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffymuerte.gif"));
    p1.setImage(imagen);
    } else if ("ICHIGO".equals(nombre1.getText())) {
    Image imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigomuerte.gif"));
    p1.setImage(imagen);
    } else if ("SNAKE".equals(nombre1.getText())) {
    Image imagen = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnakemuerte.gif"));
    p1.setImage(imagen);
    }
    
    if("LUFFY".equals(nombre2.getText())) {
    ganador = "LUFFY";
    } else if ("ICHIGO".equals(nombre2.getText())) {
    ganador = "ICHIGO";
    } else if ("SNAKE".equals(nombre2.getText())) {
    ganador = "SNAKE";
    }
    
    
    
    
    }
    }
    
    @FXML
    public void onMouseEntereda11(MouseEvent event) {
        
        at11.setScaleX(1.1);
        at11.setScaleY(1.1);
    }

    @FXML
    public void onMouseExiteda11(MouseEvent event) {

        at11.setScaleX(1.0);
        at11.setScaleY(1.0);
    }
    
    @FXML
    public void onMouseEntereda21(MouseEvent event) {


        at21.setScaleX(1.1);
        at21.setScaleY(1.1);
    }

    @FXML
    public void onMouseExiteda21(MouseEvent event) {

        at21.setScaleX(1.0);
        at21.setScaleY(1.0);
    }
    
    @FXML
    public void onMouseEnteredt1(MouseEvent event) {


        transformacion1.setScaleX(1.1);
        transformacion1.setScaleY(1.1);
    }

    @FXML
    public void onMouseExitedt1(MouseEvent event) {

        transformacion1.setScaleX(1.0);
        transformacion1.setScaleY(1.0);
    }
    
    @FXML
    public void onMouseEntereda12(MouseEvent event) {


        at12.setScaleX(1.1);
        at12.setScaleY(1.1);
    }

    @FXML
    public void onMouseExiteda12(MouseEvent event) {

        at12.setScaleX(1.0);
        at12.setScaleY(1.0);
    }
    
    @FXML
    public void onMouseEntereda22(MouseEvent event) {


        at22.setScaleX(1.1);
        at22.setScaleY(1.1);
    }

    @FXML
    public void onMouseExiteda22(MouseEvent event) {

        at22.setScaleX(1.0);
        at22.setScaleY(1.0);
    }
    
    @FXML
    public void onMouseEnteredt2(MouseEvent event) {


        transformacion2.setScaleX(1.1);
        transformacion2.setScaleY(1.1);
    }

    @FXML
    public void onMouseExitedt2(MouseEvent event) {

        transformacion2.setScaleX(1.0);
        transformacion2.setScaleY(1.0);
    }
    
    @FXML
    public void cambiarPantalla1 (MouseEvent event) throws IOException {
        FXMLLoader l = new FXMLLoader (getClass().getResource("primary_3.fxml"));
        Parent root = l.load();
        QuaternaryController controlador = l.getController();
        controlador.setEscenario(ganador);
        a.getChildren().setAll(root);
        System.gc();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }
    }