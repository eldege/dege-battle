package com.mycompany.juego;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.util.Duration;


/**
 *
 * @author Mañana_pos3
 */
public class SecondaryController implements Initializable {
    
        @FXML
    private ImageView fondo;

    @FXML
    private ImageView titulo;

    @FXML
    private ImageView luffy1;

    @FXML
    private ImageView snake1;

    @FXML
    private ImageView ichigo1;

    @FXML
    private ImageView luffy2;

    @FXML
    private ImageView snake2;

    @FXML
    private ImageView ichigo2;
    
    @FXML
    private ImageView p1;
    
    @FXML
    private ImageView p2;
    
    @FXML
    private ImageView rayos;
    
    @FXML
    private Text texto1;

    @FXML
    private Text texto2;
    
    @FXML
    private double posicion;
    
    @FXML
    private ImageView fight;
    
    @FXML
    private AnchorPane b;
    
    
    
    @FXML
    private boolean jugador1Seleccionado = false;
    
    @FXML
    private boolean jugador2Seleccionado = false;
    
    @FXML
    private Personaje personaje1;
    
    @FXML
    private Personaje personaje2;
    
    @FXML
    private double posicion1;
    
    @FXML
    private ImageView stage1;

    @FXML
    private ImageView stage2;

    @FXML
    private ImageView stage3;
    
    @FXML
    private ImageView marco1;

    @FXML
    private ImageView marco2;

    @FXML
    private ImageView marco3;
    
    private boolean isImageClicked1 = false;
    private boolean isImageClicked2 = false;
    
    private boolean isOtherImageEnabled1 = false;
    private boolean isOtherImageEnabled2 = false;
    
    private boolean isStageClicked = false;
    private boolean isStageEnabled = false;
    private boolean stage1seleccionada = false;
    private boolean stage2seleccionada = false;
    private boolean stage3seleccionada = false;
    
    @FXML
    private String escenarioElegido;
    
    @FXML
    private String personajeElegido1;
    
    @FXML
    private String personajeElegido2;
    

    
    private Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
        @FXML
    public void onMouseEnteredl1(MouseEvent event) {

        if (!isOtherImageEnabled1) {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),luffy1);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        
        Image gifImage = new Image(getClass().getResourceAsStream("/Imagenes/gifluffy.gif"));
        p1.setImage(gifImage);
        
        texto1.setText("LUFFY");
        
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p1.getFitWidth() / 2);
        p1.getTransforms().setAll(flipScale);
        }
        
        
    }
    

    @FXML
    public void onMouseExitedl1(MouseEvent event) {
        if (!isImageClicked1 && !isOtherImageEnabled1){
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),luffy1);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        
        p1.setImage(null);
        
        texto1.setText(null);
        
        Scale normalScale = new Scale(1, 1);
        normalScale.setPivotX(p1.getFitWidth() / 2);
        p1.getTransforms().setAll(normalScale);
        }
        
        
        
    }   
    
            @FXML
    public void onMouseEnteredl2(MouseEvent event) {

        if (!isOtherImageEnabled2) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),luffy2);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        
        texto2.setText("LUFFY");
        
        Image gifImage = new Image(getClass().getResourceAsStream("/Imagenes/gifluffy.gif"));
        p2.setImage(gifImage);
        }
        
    }

    @FXML
    public void onMouseExitedl2(MouseEvent event) {
        if(!isImageClicked2 && !isOtherImageEnabled2) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),luffy2);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        
        p2.setImage(null);
        
        texto2.setText(null);
        }
        
    }
    
            @FXML
    public void onMouseEntereds1(MouseEvent event) {

        if (!isOtherImageEnabled1) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),snake1);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        
        Image gifImage = new Image(getClass().getResourceAsStream("/Imagenes/gifsnake.gif"));
        posicion=p1.getTranslateX();
        p1.setTranslateX(posicion+50);
        p1.setImage(gifImage);
        
        texto1.setText("SNAKE");
        }
        
        
        
    }

    @FXML
    public void onMouseExiteds1(MouseEvent event) {
        if(!isImageClicked1 && !isOtherImageEnabled1) {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),snake1);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        
        p1.setImage(null);
        p1.setTranslateX(posicion);
        texto1.setText(null);
        }
        
        
        
    } 
    
            @FXML
    public void onMouseEntereds2(MouseEvent event) {
        if (!isOtherImageEnabled2) {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),snake2);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        
        Image gifImage = new Image(getClass().getResourceAsStream("/Imagenes/gifsnake.gif"));
        p2.setImage(gifImage);
        
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p2.getFitWidth() / 3);
        p2.getTransforms().setAll(flipScale);
        texto2.setText("SNAKE");
        }
        
        
        
    }

    @FXML
    public void onMouseExiteds2(MouseEvent event) {
        if (!isImageClicked2 && !isOtherImageEnabled2) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),snake2);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        
        p2.setImage(null);
        texto2.setText(null);
        
        Scale normalScale = new Scale(1, 1);
        normalScale.setPivotX(p2.getFitWidth() / 3);
        p2.getTransforms().setAll(normalScale);
        }
        
    } 
    
            @FXML
    public void onMouseEnteredi1(MouseEvent event) {

        if (!isOtherImageEnabled1){
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),ichigo1);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        
        Image gifImage = new Image(getClass().getResourceAsStream("/Imagenes/gifichigo.gif"));
        posicion=p1.getTranslateX();
        posicion1=p1.getTranslateY();
        p1.setTranslateX(posicion);
        p1.setTranslateY(posicion+30);
        
        p1.setFitWidth(p1.getFitWidth() * 1.2);
        p1.setFitHeight(p1.getFitHeight() * 1.2);
        
        p1.setImage(gifImage);
        texto1.setText("ICHIGO");
        }
        
    }

    @FXML
    public void onMouseExitedi1(MouseEvent event) {
        if (!isImageClicked1 && !isOtherImageEnabled1) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),ichigo1);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        
        p1.setImage(null);
        p1.setTranslateX(posicion);
        p1.setTranslateY(posicion);
        
        p1.setFitWidth(p1.getFitWidth() / 1.2);
        p1.setFitHeight(p1.getFitHeight() / 1.2);
        
        texto1.setText(null);
        }
        
    } 
    
            @FXML
    public void onMouseEnteredi2(MouseEvent event) {

        if (!isOtherImageEnabled2) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),ichigo2);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        
        Image gifImage = new Image(getClass().getResourceAsStream("/Imagenes/gifichigo.gif"));
        p2.setImage(gifImage);
        
        Scale flipScale = new Scale(-1, 1);
        flipScale.setPivotX(p2.getFitWidth() / 3);
        p2.getTransforms().setAll(flipScale);
        
        p2.setTranslateX(posicion+40);
        p2.setTranslateY(posicion+30);
        p2.setFitWidth(p2.getFitWidth() * 1.2);
        p2.setFitHeight(p2.getFitHeight() * 1.2);
        
        texto2.setText("ICHIGO");
        }
        
    }

    @FXML
    public void onMouseExitedi2(MouseEvent event) {
        if (!isImageClicked2 && !isOtherImageEnabled2) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),ichigo2);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        
        p2.setImage(null);
        
        Scale normalScale = new Scale(1, 1);
        normalScale.setPivotX(p2.getFitWidth() / 3);
        p2.getTransforms().setAll(normalScale);
        
        p2.setTranslateX(posicion);
        p2.setTranslateY(posicion);
        p2.setFitWidth(p2.getFitWidth() / 1.2);
        p2.setFitHeight(p2.getFitHeight() / 1.2);
        
        texto2.setText(null);
        }
        
    }
    
    @FXML
    public void onMouseEnteredstage1 (MouseEvent event) {
        if (!isStageEnabled) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),stage1);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        }
        
    }
    
    @FXML
    public void onMouseExitedstage1 (MouseEvent event) {
        if (!isStageEnabled && !isStageClicked) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),stage1);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        }
        
    }
    
        @FXML
    public void onMouseEnteredstage2 (MouseEvent event) {
        if (!isStageEnabled) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),stage2);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        }
        
    }
    
    @FXML
    public void onMouseExitedstage2 (MouseEvent event) {
        if (!isStageEnabled && !isStageClicked) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),stage2);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        }
    }
    
        @FXML
    public void onMouseEnteredstage3 (MouseEvent event) {
        if (!isStageEnabled) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),stage3);
        scaleTransition.setToX(1.2);
        scaleTransition.setToY(1.2);
        scaleTransition.play();
        }
    }
    
    @FXML
    public void onMouseExitedstage3 (MouseEvent event) {
        if (!isStageEnabled && !isStageClicked) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150),stage3);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
        }
    }
    
    @FXML
    public void onMouseClickedStage1 (MouseEvent event) {
    isStageEnabled = true;
    isStageClicked = true;
    stage1seleccionada = true;
    verificarSeleccionDeEscenario();
    }
    
    @FXML
    public void onMouseClickedStage2 (MouseEvent event) {
    isStageEnabled = true;
    isStageClicked = true;
    stage2seleccionada = true;
    verificarSeleccionDeEscenario();
    }
    
    @FXML
    public void onMouseClickedStage3 (MouseEvent event) {
    isStageEnabled = true;
    isStageClicked = true;
    stage3seleccionada = true;
    verificarSeleccionDeEscenario();
    }
    
    @FXML
    public void onMouseEntered(MouseEvent event) {


        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150), titulo);
        scaleTransition.setToX(1.1);
        scaleTransition.setToY(1.1);
        scaleTransition.play();
    }

    @FXML
    public void onMouseExited(MouseEvent event) {

        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150), titulo);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
    }   
    
    @FXML
    public void onMouseClicked1(MouseEvent event) {
        p1.setImage(p1.getImage());
        texto1.setText(texto1.getText());
        isImageClicked1 = true;
        isOtherImageEnabled1= true;
        jugador1Seleccionado = true;
        verificarSeleccionDePersonajes();
        
        if (texto1.getText().contains("LUFFY")) {
            personaje1 = new Personaje ("Luffy",100,0,false);
            personajeElegido1 = "Luffy";
        } else if (texto1.getText().contains("SNAKE")){
            personaje1 = new Personaje ("Snake", 100,0,false);
            personajeElegido1 = "Snake";
        } else if (texto1.getText().contains("ICHIGO")) {
            personaje1 = new Personaje ("Ichigo", 100,0,false);
            personajeElegido1 = "Ichigo";
        }
        
    }

    @FXML
    public void onMouseClicked2(MouseEvent event) {
        p2.setImage(p2.getImage());
        texto2.setText(texto2.getText());
        isImageClicked2 = true;
        isOtherImageEnabled2= true;
        jugador2Seleccionado = true;
        verificarSeleccionDePersonajes();
        
        if (texto2.getText().contains("LUFFY")) {
            personaje2 = new Personaje ("Luffy",100,0,false);
            personajeElegido2 = "Luffy";
        } else if (texto2.getText().contains("SNAKE")){
            personaje2 = new Personaje ("Snake", 100,0,false);
            personajeElegido2 = "Snake";
        } else if (texto2.getText().contains("ICHIGO")) {
            personaje2 = new Personaje ("Ichigo", 100,0,false);
            personajeElegido2 = "Ichigo";
        }
    }
    
    @FXML
    public void verificarSeleccionDePersonajes() {
    if (jugador1Seleccionado && jugador2Seleccionado) {
        marco1.setVisible(true);
        marco2.setVisible(true);
        marco3.setVisible(true);
        stage1.setVisible(true);
        stage2.setVisible(true);
        stage3.setVisible(true);
    }
    }
    
    @FXML
    public void verificarSeleccionDeEscenario() {
        if (stage1seleccionada || stage2seleccionada || stage3seleccionada) {
        rayos.setVisible(true);
        fight.setVisible(true);
        ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(1), fight);
        scaleTransition.setByX(0.1);
        scaleTransition.setByY(0.1);
        scaleTransition.setCycleCount(ScaleTransition.INDEFINITE);
        scaleTransition.setAutoReverse(true);
        scaleTransition.play();
        
        if (stage1seleccionada){
        escenarioElegido = "stage1";
        } else if (stage2seleccionada) {
        escenarioElegido = "stage2";
        } else if (stage3seleccionada) {
        escenarioElegido = "stage3";
        }
        }
    }
    
    @FXML
    public void cambiarPantalla(MouseEvent event) throws IOException {
        FXMLLoader l = new FXMLLoader (getClass().getResource("primary.fxml"));
        Parent root = l.load();
        PrimaryController controlador = l.getController();
        b.getChildren().setAll(root);
        p1.setImage(null);
        p1 = null;
        p2.setImage(null);
        p2 = null;
        System.gc();

    }
    
@FXML
    public void cambiarPantalla1 (MouseEvent event) throws IOException {
        FXMLLoader l = new FXMLLoader (getClass().getResource("primary_2.fxml"));
        Parent root = l.load();
        TertiaryController controlador = l.getController();
        controlador.setEscenario(escenarioElegido);
        controlador.setPersonajes(personajeElegido1, personajeElegido2);
        b.getChildren().setAll(root);
        p1.setImage(null);
        p1 = null;
        p2.setImage(null);
        p2 = null;
        System.gc();
    }



   

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
    
}
