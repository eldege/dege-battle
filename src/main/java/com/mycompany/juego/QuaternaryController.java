package com.mycompany.juego;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class QuaternaryController implements Initializable {

    @FXML
    private AnchorPane a;

    @FXML
    private ImageView ganador;
    
    @FXML
    private String gg;
    
    @FXML
    public void setEscenario (String gg) {
    this.gg = gg;
    
    if ("LUFFY".equals(gg)){
    Image bg = new Image(getClass().getResourceAsStream("/Imagenes/xgifluffy.gif"));
        ganador.setImage(bg);
    } else if ("SNAKE".equals(gg)) {
    Image bg = new Image(getClass().getResourceAsStream("/Imagenes/xgifsnake.gif"));
        ganador.setImage(bg);
    } else if ("ICHIGO".equals(gg)) {
    Image bg = new Image(getClass().getResourceAsStream("/Imagenes/xgifichigo.gif"));
        ganador.setImage(bg);
    }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }


}

