package com.mycompany.juego;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class PrimaryController implements Initializable {
    
private Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
@FXML
private ImageView fondo;

 @FXML
private AnchorPane a;
 
@FXML
private ImageView titulo;

@FXML
private ImageView jugar;

@FXML
    public void cambiarPantalla(MouseEvent event) throws IOException {
        // Crea una transición de opacidad
        FadeTransition fadeOut = new FadeTransition(Duration.millis(500), a);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);

        // Acción que se ejecutará cuando termine la transición de salida
        fadeOut.setOnFinished(e -> {
            try {
                // Carga la nueva pantalla
                FXMLLoader loader = new FXMLLoader(getClass().getResource("primary_1.fxml"));
                Parent root = loader.load();
                SecondaryController controller = loader.getController();

                // Establece la nueva pantalla en el mismo contenedor con transición de entrada
                a.getChildren().setAll(root);

                // Crea una transición de opacidad para la nueva pantalla
                FadeTransition fadeIn = new FadeTransition(Duration.millis(500), a);
                fadeIn.setFromValue(0.0);
                fadeIn.setToValue(1.0);
                fadeIn.play();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        // Reproduce la transición de salida
        fadeOut.play();
    }



    @FXML
    public void onMouseEntered(MouseEvent event) {


        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150), jugar);
        scaleTransition.setToX(1.1);
        scaleTransition.setToY(1.1);
        scaleTransition.play();
    }

    @FXML
    public void onMouseExited(MouseEvent event) {

        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(150), jugar);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);
        scaleTransition.play();
    }   

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
    


}
