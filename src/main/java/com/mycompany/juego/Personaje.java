/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.juego;

import javafx.fxml.FXML;

public class Personaje {
    private String nombre;
    private double vida;
    private int numeroVictorias;
    private boolean esGanador;


    public Personaje(String nombre, double vida, int numeroVictorias, boolean esGanador) {
        this.nombre = nombre;
        this.vida = vida;
        this.numeroVictorias = numeroVictorias;
        this.esGanador = esGanador;

    }

    @FXML
    public String getNombre() {
        return nombre;
    }

    @FXML
    public double getVida() {
        return vida;
    }

    // Otros métodos relacionados con el personaje, como recibir daño, atacar, etc.

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setVida(double vida) {
        this.vida = vida;
    }

    public int getNumeroVictorias() {
        return numeroVictorias;
    }

    public void setNumeroVictorias(int numeroVictorias) {
        this.numeroVictorias = numeroVictorias;
    }

    public boolean isEsGanador() {
        return esGanador;
    }

    public void setEsGanador(boolean esGanador) {
        this.esGanador = esGanador;
    }
    
    
}

